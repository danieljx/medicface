import firebase from 'firebase';
firebase.initializeApp({
    apiKey: "AIzaSyC9W8HvI0pnTzc18KUukgDxwEktYQFB85c",
    authDomain: "medicface-df55d.firebaseapp.com",
    databaseURL: "https://medicface-df55d.firebaseio.com",
    projectId: "medicface-df55d",
    storageBucket: "medicface-df55d.appspot.com",
    messagingSenderId: "144543934603",
    appId: "1:144543934603:web:6038d6ee8ac61d51040c8e",
    measurementId: "G-JXR8TVQX8F"
});
export const provider = new firebase.auth.GoogleAuthProvider();
export const auth = firebase.auth();
export const db = firebase.firestore();
export const storage = firebase.storage();
export default firebase;