import React, { Component } from 'react';
import { Modal, ModalHeader, ModalBody, Alert, ListGroup, ListGroupItem, CardImg, InputGroup, Input, InputGroupAddon, Button } from 'reactstrap';
import { storage } from '../../FirestoreConfig';
import '../../Styles/Profile.css';
import { db } from '../../FirestoreConfig';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faUser, faSearch } from '@fortawesome/free-solid-svg-icons';
export default class FriendsModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            friends: this.props.friends,
            profiles: [],
            search: false,
            urlPicture: false,
            isLoading: false,
            response: {
                error: false,
                message: false
            }
        };
        this.handleChangeSearch = this.handleChangeSearch.bind(this);
        this.getPicture = this.getPicture.bind(this);
        this.getFrinedPicture = this.getFrinedPicture.bind(this);
        this.onOpened = this.onOpened.bind(this);
        this.onClosed = this.onClosed.bind(this);
        this.setProfilesToFriends = this.setProfilesToFriends.bind(this);
    }
    handleChangeSearch(e) {
        console.log("handleChangeSearch");
        let s = e.target.value;
        if(s !== null && s !== '') {
            this.searchFriends(s);
        } else {
            this.setState({ friends: this.props.friends });
        }
        return false;
    }
    setProfilesToFriends() {
        this.setState({ friends: this.state.profiles });
    }
    toCombineProfiles() {
        console.log("toCombineProfiles");
        this.state.profiles.forEach((profile,i)=> {
            profile.friend = 0;
            this.state.friends.forEach((friend,l)=> {
                if(profile.id !== friend.id) {
                    db.collection('friends').where('friend', '==', profile.ref).get().then((querySnapshot) => {
                        querySnapshot.docs.forEach((doc) => {
                            profile.friend = doc.data().status;
                        });
                    });
                }
            });
        });
        this.setProfilesToFriends();
    }
    searchFriends(s) {
        console.log("searchFriends");
        console.log(s);
        db.collection('users').orderBy('name').startAt(s).get().then((querySnapshot) => {
            console.log(this.props.user.id);
            let usersDoc = [];
            querySnapshot.docs.forEach((doc) => {
                if(doc.id !== this.props.user.id) {
                    usersDoc.push(doc.data());
                }
            });
            console.log(usersDoc);
            this.setState({ profiles: usersDoc },()=>{
                this.toCombineProfiles();
            });
        });
    }
    onOpened() {
        console.log('onOpened');
    }
    onClosed() {
        console.log('onClosed');
    }
    getFrinedPicture(friend) {
        storage.refFromURL('gs://medicface-df55d.appspot.com/' + friend.picture).getDownloadURL().then(function(url) {
          console.log(url);
          this.setState({ urlPicture: url });
        }.bind(this)).catch(function(error) {
          console.log(error);
        });
    }
    getPicture(friend) {
        return (
            <svg className="rounded-circle mx-auto d-block img-fluid card-img bd-placeholder-img img-thumbnail" width="50" height="50" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label={friend.name + ' ' + friend.lastname}><title>{friend.name + ' ' + friend.lastname}</title><rect width="100%" height="100%" fill="#868e96"></rect><text x="30%" y="50%" fill="#dee2e6" dy=".3em">{friend.name.charAt(0) + friend.lastname.charAt(0)}</text></svg>
        )
    }
    render() {
        return (
            <Modal className="modal-lg" isOpen={this.props.modal} toggle={this.props.toggleModal} onOpened={this.onOpened} onClosed={this.onClosed}>
                <ModalHeader toggle={this.props.toggleModal}>Friends List</ModalHeader>
                <ModalBody>
                    { this.state.response.error &&
                        <Alert color="danger">{this.state.response.message}</Alert>
                    }
                    <InputGroup>
                        <Input placeholder="Search" name="search" onChange={this.handleChangeSearch} />
                        <InputGroupAddon addonType="prepend">
                            <span className="input-group-text">
                                <FontAwesomeIcon icon={faSearch} />
                            </span>
                        </InputGroupAddon>
                    </InputGroup>
                    <ListGroup className="list-group-flush">
                        {this.state.friends.map((friend, index) => {
                            this.getFrinedPicture(friend);
                            return (
                                <ListGroupItem key={index}>
                                    <div className="row w-100">
                                        <div className="col-12 col-sm-6 col-md-2 px-0">
                                            {
                                                this.state.urlPicture ? (
                                                    <CardImg className="rounded-circle mx-auto d-block img-fluid" src={this.state.urlPicture} alt={this.friend.name + ' ' + this.friend.lastname}/>
                                                ):(
                                                    this.getPicture(friend)
                                                )
                                            }
                                        </div>
                                        <div className="contentName col-12 col-sm-6 col-md-8 text-center text-sm-left">
                                            <label className="name lead">{friend.name + ' ' + friend.lastname}</label>
                                        </div>
                                        <div className="contentAction col-12 col-sm-6 col-md-2 text-center text-sm-left">
                                            <Button color={(friend.friend===3?"success":(friend.friend===2?"warning":"primary"))} className="float-right" id="statusFriend" onClick={this.toggleModal} disabled>
                                                <FontAwesomeIcon icon={faUser} />
                                            </Button>
                                        </div>
                                    </div>
                                </ListGroupItem>
                            )
                        })}
                    </ListGroup>
                </ModalBody>
            </Modal>
        )
    }
}