import React, { Component } from 'react';
import { Card, CardHeader, Button, CardFooter, ListGroup, UncontrolledTooltip, CardImg } from 'reactstrap';
import { Link } from "react-router-dom";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlus } from '@fortawesome/free-solid-svg-icons';
import '../../Styles/Profile.css';
import { storage } from '../../FirestoreConfig';
import FriendsModal from './FriendsModal';
export default class FriendsBox extends Component {
    constructor(props) {
        super(props);
        this.state = {
            friends: this.props.friends,
            urlPicture: false,
            modal: false,
            isOpen: false
        }
        this.toggleModal = this.toggleModal.bind(this);
        this.getPicture = this.getPicture.bind(this);
        this.getFrinedPicture = this.getFrinedPicture.bind(this);
    }
    toggleModal(e) {
        e.preventDefault();
        console.log(this.state);
        this.setState({
            modal: !this.state.modal
        });
        return false;
    }
    getFrinedPicture(friend) {
        storage.refFromURL('gs://medicface-df55d.appspot.com/' + friend.picture).getDownloadURL().then(function(url) {
          console.log(url);
          this.setState({ urlPicture: url });
        }.bind(this)).catch(function(error) {
          console.log(error);
        });
    }
    getPicture(friend) {
        return (
            <svg className="rounded-circle mx-auto d-block img-fluid card-img bd-placeholder-img img-thumbnail" width="200" height="200" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label={friend.name + ' ' + friend.lastname}><title>{friend.name + ' ' + friend.lastname}</title><rect width="100%" height="100%" fill="#868e96"></rect><text x="30%" y="50%" fill="#dee2e6" dy=".3em">{friend.name.charAt(0) + friend.lastname.charAt(0)}</text></svg>
        )
    }
    render() {
        return (
            <Card className="contacts-card">
                <CardHeader>
                    Friends
                    <Button color="secondary" size="sm" className="float-right" id="addFriends" onClick={this.toggleModal}>
                        <FontAwesomeIcon icon={faPlus} />
                    </Button>
                    <UncontrolledTooltip placement="right" target="addFriends">
                        Add Friends
                    </UncontrolledTooltip>
                    <FriendsModal user={this.props.user} friends={this.props.friends} modal={this.state.modal} toggleModal={this.toggleModal} />
                </CardHeader>
                <ListGroup className="list-group-flush">
                    {this.props.friends.map((friend, index) => {
                        this.getFrinedPicture(friend);
                        return (
                            <Link key={index} to="/profile" className="list-group-item-action list-group-item">
                                <div className="row w-100">
                                    <div className="col-12 col-sm-6 col-md-2 px-0">
                                        {
                                            this.state.urlPicture ? (
                                                <CardImg className="rounded-circle mx-auto d-block img-fluid" src={this.state.urlPicture} alt={this.friend.name + ' ' + this.friend.lastname}/>
                                            ):(
                                                this.getPicture(friend)
                                            )
                                        }
                                    </div>
                                    <div className="col-12 col-sm-6 col-md-10 text-center text-sm-left">
                                        <label className="name lead">{friend.name + ' ' + friend.lastname}</label>
                                    </div>
                                </div>
                            </Link>
                        )
                    })}
                </ListGroup>
                <CardFooter>
                    <Button color="primary" size="sm" block>View All</Button>
                </CardFooter>
            </Card>
        )
    }
}