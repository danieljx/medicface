import React from 'react';
import { Row, Col, Container } from 'reactstrap';

export default () => {
    return (
        <footer>
            <Container>
                <Row className="d-flex justify-content-center">
                    <Col className="text-center" xs="12" sm="12" md="12">
                        <p className="h6">By:<a className="text-green ml-2" href="https://elvilla.net" target="_blank">@danieljx</a></p>
                    </Col>
                </Row>
            </Container>
        </footer>
    )
}