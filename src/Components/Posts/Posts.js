import React, { Component } from 'react';
import { Container } from 'reactstrap';
import Post from './Post';
export default class Posts extends Component {
    constructor(props) {
        super(props);
        this.state = {
            posts: this.props.posts
        };
    }
    render() {
        return (
            <Container>
                {this.props.posts.map((post, index) => {
                    return <Post key={index} post={post} deletePost={this.props.deletePost} />
                })}
            </Container>
        )
    }
}