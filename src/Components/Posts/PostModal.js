import React, { Component } from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter, Input, Form, FormGroup, InputGroup, InputGroupAddon, InputGroupText, Alert } from 'reactstrap';
import { db } from '../../FirestoreConfig';
export default class PostModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            title: false,
            content: false,
            response: false,
            isLoading: false
        };
        this.sentPost = this.sentPost.bind(this);
        this.handleChangeTitle = this.handleChangeTitle.bind(this);
        this.handleChangeContent = this.handleChangeContent.bind(this);
        this.onOpened = this.onOpened.bind(this);
        this.onClosed = this.onClosed.bind(this);
    }
    sentPost() {
        console.log("sentPost");
        this.setState({ isLoading: true });
        let dataPost = {
            title: this.state.title,
            content: this.state.content,
            date_create: Date(),
            date_update: Date(),
            user: this.props.user.ref
        };
        db.collection("posts").add(dataPost).then((resp) => {
            console.log(resp);
            dataPost.id = resp.id;
            console.log("Document successfully written!");
            this.setState({ isLoading: false });
            this.props.addPost(dataPost);
            this.props.toggleModal();
        }).catch((error) => {
            console.log(error);
            this.setState({response: {
                error: true,
                message: error.message
            }});
            this.setState({ isLoading: false });
        });
        return false;
    }
    handleChangeTitle(e) {
        console.log("handleChangeTitle");
        this.setState({ title: e.target.value });
        return false;
    }
    handleChangeContent(e) {
        console.log("handleChangeContent");
        this.setState({ content: e.target.value });
        return false;
    }
    onOpened() {
        console.log('onOpened');
    }
    onClosed() {
        console.log('onClosed');
        this.setState({
            title: false,
            content: false
        });
    }
    render() {
        return (
            <Modal isOpen={this.props.modal} toggle={this.props.toggleModal} onOpened={this.onOpened} onClosed={this.onClosed}>
                <ModalHeader toggle={this.props.toggleModal}>New Post</ModalHeader>
                <ModalBody>
                        { this.state.response &&
                            <Alert color={this.state.response.error?"danger":"success"}>{this.state.response.message}</Alert>
                        }
                    <Form>
                        <FormGroup>
                            <InputGroup>
                                <InputGroupAddon addonType="prepend">
                                    <InputGroupText>
                                        Title:
                                    </InputGroupText>
                                </InputGroupAddon>
                                <Input type="text" id="title" name="title" placeholder="Title something.." onChange={this.handleChangeTitle} disabled={this.state.isLoading}/>
                            </InputGroup>
                        </FormGroup>
                        <FormGroup>
                            <InputGroup>
                                <InputGroupAddon addonType="prepend">
                                    <InputGroupText>
                                        Content:
                                    </InputGroupText>
                                </InputGroupAddon>
                                <Input type="textarea" id="content" name="content" placeholder="Write something.." rows={5} onChange={this.handleChangeContent} disabled={this.state.isLoading}/>
                            </InputGroup>
                        </FormGroup>
                    </Form>
                </ModalBody>
                <ModalFooter>
                    <Button color="primary" onClick={this.sentPost} disabled={this.state.isLoading}>Save</Button>
                    <Button color="secondary" onClick={this.props.toggleModal}>Cancel</Button>
                </ModalFooter>
            </Modal>
        )
    }
}