import React, { Component } from 'react';
import { Card, CardText, CardHeader, CardBody, CardFooter, CardTitle, Button, Modal, ModalHeader, ModalBody, ModalFooter, Alert } from 'reactstrap';
import '../../Styles/Profile.css';
import Moment from 'react-moment';
import 'moment-timezone';
import Parser from 'html-react-parser';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTrashAlt } from '@fortawesome/free-solid-svg-icons';
import { db } from '../../FirestoreConfig';
export default class Posts extends Component {
    constructor(props) {
        super(props);
        this.state = {
            post: this.props.post,
            modal: false,
            isOpen: false
        };
        this.deletePost = this.deletePost.bind(this);
        this.deleteConfirm = this.deleteConfirm.bind(this);
        this.toggleModal = this.toggleModal.bind(this);
    }
    toggleModal() {
        console.log(this.state);
        this.setState({
            modal: !this.state.modal
        });
    }
    deletePost(e) {
        e.preventDefault();
        console.log('deletePost');
        db.collection("posts").doc(this.state.post.id).delete().then(function() {
            console.log("Document successfully deleted!");
            this.props.deletePost(this.state.post.id);
        }.bind(this)).catch(function(error) {
            console.error("Error removing document: ", error);
        });
        return false;
    }
    deleteConfirm(e) {
        e.preventDefault();
        this.toggleModal();
        return false;
    }
    render() {
        return (
            <Card>
                <CardHeader>
                    <small className="text-muted">By: {this.props.post.user.name + ' ' + this.props.post.user.lastname}</small>
                    <a href="#" className="float-right" onClick={this.deleteConfirm}>
                        <small className="text-muted">
                            <FontAwesomeIcon icon={faTrashAlt}/>
                        </small>
                    </a>
                </CardHeader>
                <CardBody>
                    <CardTitle>{this.props.post.title}</CardTitle>
                    <CardText>{Parser(this.props.post.content)}</CardText>
                </CardBody>
                <CardFooter>
                    <CardText>
                        <small className="text-muted">Last updated <Moment fromNow>{this.props.post.date_update}</Moment></small>
                    </CardText>
                </CardFooter>
                <Modal isOpen={this.state.modal} toggle={this.toggleModal}>
                    <ModalHeader toggle={this.toggleModal}>Delete Post</ModalHeader>
                    <ModalBody>
                            { this.state.response &&
                                <Alert color={this.state.response.error?"danger":"success"}>{this.state.response.message}</Alert>
                            }
                            <p>Are you sure you want to delete this post "{this.props.post.title}"?</p>
                    </ModalBody>
                    <ModalFooter>
                        <Button color="danger" onClick={this.deletePost} disabled={this.state.isLoading}>Yes</Button>
                        <Button color="secondary" onClick={this.toggleModal}>Cancel</Button>
                    </ModalFooter>
                </Modal>
            </Card>
        )
    }
}