import React, { Component } from 'react';
import {  
    Collapse,
    Navbar,
    NavbarToggler,
    NavbarBrand,
    Nav,
    NavItem,
    NavLink,
    UncontrolledTooltip
} from 'reactstrap';
import { Link } from "react-router-dom";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faUsers, faPlus, faSignOutAlt } from '@fortawesome/free-solid-svg-icons';
import PostModal from './Posts/PostModal';
export default class Header extends Component {
    constructor(props) {
        super(props);
        this.state = {
            user: this.props.user,
            isAuthenticated: this.props.isAuthenticated,
            modal: false,
            isOpen: false
        };
        this.toggleModal = this.toggleModal.bind(this);
        this.toggleNav = this.toggleNav.bind(this);
        this.getPicture = this.getPicture.bind(this);
    }
    toggleModal() {
        console.log(this.state);
        this.setState({
            modal: !this.state.modal
        });
    }
    toggleNav() {
        console.log(this.state);
        this.setState({
            isOpen: !this.state.isOpen
        });
    }
    getPicture() {
        return (
            <svg className="bd-placeholder-img img-thumbnail" width="25" height="25" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label="A generic square placeholder image with a white border around it, making it resemble a photograph taken with an old instant camera: 200x200"><title>A generic square placeholder image with a white border around it, making it resemble a photograph taken with an old instant camera</title><rect width="100%" height="100%" fill="#868e96"></rect><text x="50%" y="50%" fill="#dee2e6" dy=".3em">{this.state.user.name.charAt(0) + this.state.user.lastname.charAt(0)}</text></svg>
        )
    }
    render() {
        return (
            <Navbar color="blue" dark expand="md" fixed="top">
                <NavbarBrand href="/">MedicFace</NavbarBrand>
                <NavbarToggler onClick={this.toggleNav} />
                <Collapse isOpen={this.state.isOpen} navbar>
                    <Nav className="ml-auto" navbar>
                        <form className="form-inline">
                            <input className="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search" />
                        </form>
                        <NavItem>
                            <NavLink href="#" role="button" onClick={this.toggleModal}>
                                <FontAwesomeIcon icon={faPlus} />
                                Post
                            </NavLink>
                            <PostModal user={this.props.user} addPost={this.props.addPost} modal={this.state.modal} toggleModal={this.toggleModal} />
                        </NavItem>
                        <NavItem>
                            <Link to="/friends" role="button" className="nav-link">
                                <FontAwesomeIcon icon={faUsers} className="icon" />
                                Friends
                            </Link>
                        </NavItem>
                        <NavItem>
                            <Link to="/profile" className="nav-link">
                                {
                                    this.props.urlPicture ? (
                                        <img className="d-inline-block align-top" width="25" height="25" src={this.props.urlPicture} alt={this.state.user.name + ' ' + this.state.user.lastname}/>
                                    ):(
                                        this.getPicture()
                                    )
                                }
                                {this.state.user.name}
                            </Link>
                        </NavItem>
                        <NavItem>
                            <NavLink id="signOut" href="#" role="button" onClick={this.props.logOut}>
                                <FontAwesomeIcon icon={faSignOutAlt} />
                            </NavLink>
                            <UncontrolledTooltip placement="bottom" target="signOut">
                                Signout
                            </UncontrolledTooltip>
                        </NavItem>
                    </Nav>
                </Collapse>
            </Navbar>
        )
    }
}