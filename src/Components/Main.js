import React, { Component } from 'react';
import { Row, Col, Container } from 'reactstrap';
import Header from './Header';
import ProfileBox from './Profiles/ProfileBox';
import FriendsBox from './Friends/FriendsBox';
import Posts from './Posts/Posts';
export default class Main extends Component {
    constructor(props) {
        super(props);
        this.state = {
            user: this.props.user
        }
    }
    render() {
        return (
            <Row>
                <Col xs="12" sm="12" md="12">
                    <Header addPost={this.props.addPost} user={this.state.user} logOut={this.props.logOut} urlPicture={this.props.urlPicture} urlWallpaper={this.props.urlWallpaper}/>
                </Col>
                <Container fluid >
                    <Row>
                        <Col xs="12" sm="12" md="4">
                            <Container>
                                <ProfileBox countFriends={this.props.friends.length} countPosts={this.props.posts.length} user={this.props.user} lasLogin={this.props.lasLogin} urlPicture={this.props.urlPicture} urlWallpaper={this.props.urlWallpaper}/>
                                <FriendsBox user={this.state.user} friends={this.props.friends} />
                            </Container>
                        </Col>
                        <Col xs="12" sm="12" md="8">
                            <Posts deletePost={this.props.deletePost} posts={this.props.posts} />
                        </Col>
                    </Row>
                </Container>
            </Row>
        )
    }
}