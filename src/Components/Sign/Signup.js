import React, { Component } from 'react';
import '../../Styles/Sign.css';
import { db, auth, storage } from '../../FirestoreConfig';
import { Row, Col, Container, Button, Form, FormGroup, Input, ButtonGroup, InputGroup, InputGroupAddon, InputGroupText, FormText, Alert } from 'reactstrap';
import { Link } from "react-router-dom";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowLeft, faSignInAlt } from '@fortawesome/free-solid-svg-icons';
export default class Signup extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: false,
            password: false,
            name: false,
            lastname: false,
            phone: false,
            location: false,
            picture: false,
            wallpaper: false,
            fPicture: false,
            fWallpaper: false,
            response: false
        };
        this.setUploadPicture = this.setUploadPicture.bind(this);
        this.setUploadWallpaper = this.setUploadWallpaper.bind(this);
        this.setCreateAuth = this.setCreateAuth.bind(this);
        this.setRegister = this.setRegister.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.onChangePicture = this.onChangePicture.bind(this);
        this.onChangeWallpaper = this.onChangeWallpaper.bind(this);
    }
    setUploadPicture() {
        if(this.state.fPicture != null) {
            var storageRef = storage.ref();
            var imgRef = storageRef.child('img/' + this.state.email + '/' + this.state.fPicture.name);
            imgRef.put(this.state.fPicture).then((snapshot) => {
                console.log('Uploaded a blob or setUploadPicture!');
                console.log(snapshot);
                this.setState({ picture: snapshot.metadata.fullPath });
                this.setUploadWallpaper();
            }).catch((error) => {
                console.log(error);
                this.setState({response: {
                  error: true,
                  message: error.message
                }});
            });
        } else {
            this.setRegister();
        }
    }
    setUploadWallpaper() {
        if(this.state.fWallpaper != null) {
            var storageRef = storage.ref();
            var imgRef = storageRef.child('img/' + this.state.email + '/' + this.state.fWallpaper.name);
            imgRef.put(this.state.fWallpaper).then((snapshot) => {
                console.log('Uploaded a blob or setUploadWallpaper!');
                console.log(snapshot);
                this.setState({ wallpaper: snapshot.metadata.fullPath });
                this.setRegister();
            }).catch((error) => {
                console.log(error);
                this.setState({response: {
                  error: true,
                  message: error.message
                }});
            });
        } else {
            this.setRegister();
        }
    }
    setCreateAuth() {
        console.log('setCreateAuth');
        console.log(this.state);
        auth.createUserWithEmailAndPassword(this.state.email, this.state.password).then((resp) => {
            console.log(resp);
            console.log("createUserWithEmailAndPassword successfully written!");
            this.setUploadPicture();
        }).catch((error) => {
            console.log(error);
            this.setState({response: {
              error: true,
              message: error.message
            }});
        });
    }
    setRegister() {
        console.log('setRegister');
        db.collection("users").add({
            email: this.state.email,
            password: this.state.password,
            name: this.state.name,
            lastname: this.state.lastname,
            phone: this.state.phone,
            location: this.state.location,
            picture: this.state.picture,
            wallpaper: this.state.wallpaper,
            date_create: Date(),
            date_update: Date(),
            last_login: null
        }).then((resp) => {
            console.log(resp);
            console.log("Document successfully written!");
        }).catch((error) => {
            console.log(error);
            this.setState({response: {
                error: true,
                message: error.message
            }});
        });
    }
    handleSubmit(e) {
        e.preventDefault();
        console.log("handleSubmit");
        const formToJSON = elements => [].reduce.call(elements, (data, element) => {
            if(element.name !== "") {
                data[element.name] = element.value;
            }
            return data;
        }, {});
        let form = e.target;
        let data = formToJSON(form.elements);
        console.log(data.email);
        this.setState({ email: data.email });
        console.log(this.state.email);
        this.setState({
            email: data.email,
            password: data.password,
            name: data.name,
            lastname: data.lastname,
            phone: data.phone,
            location: data.location
        },() => {
            this.setCreateAuth();
        });
        return false;
    }
    onChangePicture(e) {
        console.log(e.target.files);
        if(e.target.files.length > 0) {
            this.setState({ fPicture: e.target.files[0] });
        }
    }
    onChangeWallpaper(e) {
        console.log(e.target.files);
        if(e.target.files.length > 0) {
            this.setState({ fWallpaper: e.target.files[0] });
        }
    }
    render() {
        return (
            <Container className="login-container">
                <Row>
                    <Col md="6" className="login-form">
                        <h3>MedicFace</h3>
                        { this.state.response &&
                            <Alert color={this.state.response.error?"danger":"success"}>{this.state.response.message}</Alert>
                        }
                        <Form onSubmit={this.handleSubmit}>
                            <FormGroup>
                                <InputGroup>
                                    <InputGroupAddon addonType="prepend">
                                        <InputGroupText>
                                            E-mail:
                                        </InputGroupText>
                                    </InputGroupAddon>
                                    <Input type="email" name="email" placeholder="example@example.com" />
                                </InputGroup>
                            </FormGroup>
                            <FormGroup>
                                <InputGroup>
                                    <InputGroupAddon addonType="prepend">
                                        <InputGroupText>
                                            Password:
                                        </InputGroupText>
                                    </InputGroupAddon>
                                    <Input type="password" name="password" placeholder="password" />
                                </InputGroup>
                            </FormGroup>
                            <FormGroup>
                                <InputGroup>
                                    <InputGroupAddon addonType="prepend">
                                        <InputGroupText>
                                            Name: 
                                        </InputGroupText>
                                    </InputGroupAddon>
                                    <Input type="text" name="name" placeholder="Name" />
                                </InputGroup>
                            </FormGroup>
                            <FormGroup>
                                <InputGroup>
                                    <InputGroupAddon addonType="prepend">
                                        <InputGroupText>
                                            Last Name: 
                                        </InputGroupText>
                                    </InputGroupAddon>
                                    <Input type="text" name="lastname" placeholder="Last Name" />
                                </InputGroup>
                            </FormGroup>
                            <FormGroup>
                                <InputGroup>
                                    <InputGroupAddon addonType="prepend">
                                        <InputGroupText>
                                            Phone: 
                                        </InputGroupText>
                                    </InputGroupAddon>
                                    <Input type="text" name="phone" placeholder="(+5#) ### ####" />
                                </InputGroup>
                            </FormGroup>
                            <FormGroup>
                                <InputGroup>
                                    <InputGroupAddon addonType="prepend">
                                        <InputGroupText>
                                            Location: 
                                        </InputGroupText>
                                    </InputGroupAddon>
                                    <Input type="text" name="location" placeholder="City, Country" />
                                </InputGroup>
                            </FormGroup>
                            <FormGroup>
                                <InputGroup>
                                    <InputGroupAddon addonType="prepend">
                                        <InputGroupText>
                                            Picture: 
                                        </InputGroupText>
                                    </InputGroupAddon>
                                    <Input type="file" name="picture" onChange={this.onChangePicture} />
                                    <FormText color="muted">
                                        Picture profile.
                                    </FormText>
                                </InputGroup>
                            </FormGroup>
                            <FormGroup>
                                <InputGroup>
                                    <InputGroupAddon addonType="prepend">
                                        <InputGroupText>
                                            Wallpaper: 
                                        </InputGroupText>
                                    </InputGroupAddon>
                                    <Input type="file" name="wallpaper" onChange={this.onChangeWallpaper} />
                                    <FormText color="muted">
                                        Wallpaper profile.
                                    </FormText>
                                </InputGroup>
                            </FormGroup>
                            <FormGroup>
                                <ButtonGroup>
                                    <Button className="btnSubmit">
                                        <span>Register</span>
                                        <FontAwesomeIcon icon={faSignInAlt} className="iconToRegister" />
                                    </Button>
                                </ButtonGroup>
                            </FormGroup>
                            <FormGroup>
                                <FontAwesomeIcon icon={faArrowLeft} className="backToLogin" />
                                <Link to="/login" className="btnBackToLogin">
                                    Back Login
                                </Link>
                            </FormGroup>
                        </Form>
                    </Col>
                </Row>
            </Container>
        )
    }
}