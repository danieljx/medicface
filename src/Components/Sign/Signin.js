import React, { Component } from 'react';
import '../../Styles/Sign.css';
import { Row, Col, Container, Button, Form, FormGroup, Input, ButtonGroup, InputGroup, InputGroupAddon, InputGroupText, Alert } from 'reactstrap';
import { Link } from "react-router-dom";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSignInAlt, faLock, faUser } from '@fortawesome/free-solid-svg-icons';
export default class Signin extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: false,
            pass: false
        };
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChangeEmail = this.handleChangeEmail.bind(this);
        this.handleChangePass = this.handleChangePass.bind(this);
        this.onLoginGoogle = this.onLoginGoogle.bind(this);
        this.onLogin = this.onLogin.bind(this);
    }
    onLoginGoogle() {
        console.log('onLoginGoogle');
        this.props.loginGoogle();
    }
    onLogin() {
        console.log('onLogin');
        console.log(this.props.login);
        this.props.login(this.state.email, this.state.pass);
    }
    handleSubmit(e) {
        console.log("handleSubmit");
        e.preventDefault();
        this.onLogin();
        return false;
    }
    handleChangeEmail(e) {
        console.log("handleChangeEmail");
        this.setState({ email: e.target.value });
        return false;
    }
    handleChangePass(e) {
        console.log("handleChangePass");
        this.setState({ pass: e.target.value });
        return false;
    }
    render() {
        return (
            <Container className="login-container">
                <Row>
                    <Col md="6" className="login-form">
                        <h3>MedicFace</h3>
                        { this.props.response &&
                            <Alert color={this.props.response.error?"danger":"success"}>{this.props.response.message}</Alert>
                        }
                        <Form onSubmit={this.handleSubmit}>
                            <FormGroup>
                                <InputGroup>
                                    <InputGroupAddon addonType="prepend">
                                        <InputGroupText>
                                            <FontAwesomeIcon icon={faUser} />
                                        </InputGroupText>
                                    </InputGroupAddon>
                                    <Input type="email" id="email" name="email" placeholder="example@example.com" onChange={this.handleChangeEmail} />
                                </InputGroup>
                            </FormGroup>
                            <FormGroup>
                                <InputGroup>
                                    <InputGroupAddon addonType="prepend">
                                        <InputGroupText>
                                            <FontAwesomeIcon icon={faLock} />
                                        </InputGroupText>
                                    </InputGroupAddon>
                                    <Input type="password" id="password" name="password" placeholder="password" onChange={this.handleChangePass} />
                                </InputGroup>
                            </FormGroup>
                            <FormGroup>
                                <ButtonGroup>
                                    <Button className="btnSubmit">
                                        <span>Login</span>
                                        <FontAwesomeIcon icon={faSignInAlt} className="iconToLogin" />
                                    </Button>
                                </ButtonGroup>
                            </FormGroup>
                            <FormGroup>
                                <ButtonGroup>
                                    <Button className="btnLoginGoogle" onClick={this.onLoginGoogle}>
                                        <span>Login with Google</span>
                                    </Button>
                                </ButtonGroup>
                            </FormGroup>
                            <FormGroup>
                                <Link to="/register" className="btnRegister">Register</Link>
                            </FormGroup>
                        </Form>
                    </Col>
                </Row>
            </Container>
        )
    }
}