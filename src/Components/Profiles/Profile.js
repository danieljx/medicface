import React from 'react';
import { Container, Card, CardImg, CardText, CardBody, CardTitle } from 'reactstrap';
import '../../Styles/Profile.css';

export default () => {
    return (
        <Container>
            <Card className="profile-card">
                <div className="card-img-block">
                    <CardImg top className="img-fluid" width="100%" src="/img/mebg.jpeg" alt="Card image cap" />
                </div>
                <CardBody>
                    <CardImg width="100%" className="profile" src="/img/me.jpg" alt="Daniel Villanueva" />
                    <CardTitle className="text-center">Daniel Villanueva</CardTitle>
                    <CardText>
                        <small className="text-muted">45 Followers, 13 Posts</small>
                    </CardText>
                </CardBody>
            </Card>
        </Container>
    )
}