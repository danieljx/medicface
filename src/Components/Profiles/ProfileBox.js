import React, { Component } from 'react';
import { Card, CardImg, CardText, CardBody, CardTitle } from 'reactstrap';
import '../../Styles/Profile.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEnvelope, faPhoneAlt, faMapMarkerAlt } from '@fortawesome/free-solid-svg-icons';
import Moment from 'react-moment';
import 'moment-timezone';
export default class ProfileBox extends Component {
    constructor(props) {
        super(props);
        this.state = {
            user: this.props.user
        }
        this.getPicture = this.getPicture.bind(this);
        this.getWallpaper = this.getWallpaper.bind(this);
    }
    getPicture() {
        return (
            <svg className="profile card-img bd-placeholder-img img-thumbnail" width="200" height="200" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label={this.state.user.name + ' ' + this.state.user.lastname}><title>{this.state.user.name + ' ' + this.state.user.lastname}</title><rect width="100%" height="100%" fill="#868e96"></rect><text x="40%" y="50%" fill="#dee2e6" dy=".3em">{this.state.user.name.charAt(0) + this.state.user.lastname.charAt(0)}</text></svg>
        )
    }
    getWallpaper() {
        return (
            <svg className="img-fluid card-img bd-placeholder-img bd-placeholder-img-lg img-fluid" width="100%" height="250" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label={this.state.user.name + ' ' + this.state.user.lastname}><title>Placeholder</title><rect width="100%" height="100%" fill="#868e96"></rect><text x="35%" y="50%" fill="#dee2e6" dy=".3em">{this.state.user.name + ' ' + this.state.user.lastname}</text></svg>
        )
    }
    render() {
        return (
            <Card className="profile-card">
                <div className="card-img-block">
                    {
                        this.props.urlWallpaper ? (
                            <CardImg top className="img-fluid" width="100%" src={this.props.urlWallpaper} alt={this.state.user.name + ' ' + this.state.user.lastname}/>
                        ):(
                            this.getWallpaper()
                        )
                    }
                </div>
                <CardBody>
                    {
                        this.props.urlPicture ? (
                            <CardImg width="100%" className="profile" src={this.props.urlPicture} alt={this.state.user.name + ' ' + this.state.user.lastname}/>
                        ):(
                            this.getPicture()
                        )
                    }
                    <CardTitle className="text-center">{this.state.user.name + ' ' + this.state.user.lastname}</CardTitle>
                    <CardText>
                        <FontAwesomeIcon icon={faEnvelope}  className="text-muted" />
                        <span className="text-muted">{this.state.user.email}</span>
                    </CardText>
                    <CardText>
                        <FontAwesomeIcon icon={faPhoneAlt}  className="text-muted" />
                        <span className="text-muted">{this.state.user.phone}</span>
                    </CardText>
                    <CardText>
                        <FontAwesomeIcon icon={faMapMarkerAlt}  className="text-muted" />
                        <span className="text-muted">{this.state.user.location}</span>
                    </CardText>
                    <CardText>
                        <small className="text-muted">{this.props.countFriends} Friends, {this.props.countPosts} Posts</small>
                    </CardText>
                    <CardText>
                        <small className="text-muted">Last Login: <Moment fromNow>{this.props.lastLogin}</Moment></small>
                    </CardText>
                </CardBody>
            </Card>
        )
    }
}