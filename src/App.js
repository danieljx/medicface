import React, { Component } from 'react';
import { Switch, Route, Redirect } from "react-router-dom";
import { Container } from 'reactstrap';
import Signin from './Components/Sign/Signin';
import Signup from './Components/Sign/Signup';
import Main from './Components/Main';
import Footer from './Components/Footer';
import './Styles/App.css';
import { auth, db, provider, storage } from './FirestoreConfig';
import Loading from './Components/Loading';
class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      isAuthenticated: false,
      user: false,
      urlPicture: false,
      urlWallpaper: false,
      response: false,
      lasLogin: false,
      posts: [],
      friends: []
    };
    this.loginGoogle = this.loginGoogle.bind(this);
    this.login = this.login.bind(this);
    this.logOut = this.logOut.bind(this);
    this.getSession = this.getSession.bind(this);
    this.getUserFromSession = this.getUserFromSession.bind(this);
    this.getPosts = this.getPosts.bind(this);
    this.addPost = this.addPost.bind(this);
    this.deletePost = this.deletePost.bind(this);
    this.getFriends = this.getFriends.bind(this);
  }
  componentDidMount() {
    console.log('componentDidMount');
    this.getSession();
    console.log('componentDidMount end');
    //this.setState({ isLoading: false });
  }
  addPost(post) {
    post.user = this.state.user;
    let postData = this.state.posts;
    postData.unshift(post)
    this.setState({ posts: postData });
  }
  deletePost(id) {
    this.setState(prevState => ({
      posts: prevState.posts.filter(el => el.id !== id )
    }));
  }
  getPosts(refUser) {
    console.log('getPosts');
    console.log(refUser);
    let queryPost = db.collection('posts').where('user', '==', refUser).where('status', '==', 1).orderBy("date_create");
    console.log(queryPost);
    queryPost.get().then((querySnapshot) => {
      let postsDoc = querySnapshot.docs.map((doc) => {
        let post = doc.data();
            post.id = doc.id;
            post.user = this.state.user;
        return post;
      });
      console.log(postsDoc);
      this.setState({ posts: postsDoc });
    });
  }
  getFriends(refUser) {
    console.log('getFriends');
    console.log(refUser);
    db.collection('friends').where('user', '==', refUser).where('status', '==', 3).get().then((querySnapshot) => {
      querySnapshot.docs.map((doc) => {
        doc.data().friend.get().then((querySnapshotFriend) => {
          console.log(querySnapshotFriend);
          console.log(querySnapshotFriend.data());
          let f = querySnapshotFriend.data();
              f.id = doc.id;
              f.ref = doc.ref;
              f.friend = 3;
          let friendData = this.state.friends;
              friendData.unshift(f);
              this.setState({ friends: friendData },()=>{
                console.log(this.state.friends);
              });
        });
        return doc.data();
      });
    });
  }
  getSession() {
    this.setState({ isLoading: true });
    auth.onAuthStateChanged((user) => {
      if (user) {
        console.log(user.email);
        this.getUserFromSession(user);
      } else {
        this.setState({ isLoading: false });
      }
    });
  }
  getUserFromSession(user) {
    db.collection("users").where("email", "==", user.email).get().then((querySnapshot) => {
      console.log(querySnapshot);
      console.log(querySnapshot.docs);
      let userDoc = querySnapshot.docs.map((doc) => {
        this.setState({ lasLogin: doc.data().last_login });
        db.collection('users').doc(doc.id).update({
          last_login: Date()
        });
        this.getPosts(doc.ref);
        this.getFriends(doc.ref);
        let docUser = doc.data();
            docUser.id = doc.id;
            docUser.ref = doc.ref;
        return docUser;
      });
      console.log(userDoc.length);
      console.log(userDoc);
      if(userDoc.length) {
        console.log(userDoc[0]);
        this.setState({ user: userDoc[0] });
        console.log(userDoc[0].picture);
        storage.refFromURL('gs://medicface-df55d.appspot.com/' + userDoc[0].picture).getDownloadURL().then(function(url) {
          console.log(url);
          this.setState({ urlPicture: url });
        }.bind(this)).catch(function(error) {
          console.log(error);
        });
        storage.refFromURL('gs://medicface-df55d.appspot.com/' + userDoc[0].wallpaper).getDownloadURL().then(function(url) {
          console.log(url);
          this.setState({ urlWallpaper: url });
        }.bind(this)).catch(function(error) {
          console.log(error);
        });
      }
      this.setState({ isAuthenticated: true });
      this.setState({ isLoading: false });
    });
  }
  loginGoogle() {
    console.log('loginGoogle');
    auth.signInWithPopup(provider).then((result) => {
        console.log('signInWithPopup');
        this.getUserFromSession(result.user);
    }).catch((error) => {
        console.log(error);
        this.setState({response: {
          error: true,
          message: error.message
        }});
    });
  }
  login(email, pass) {
    console.log('login');
    auth.signInWithEmailAndPassword(email, pass).then((result) => {
        this.setState({response: {
          error: false,
          message: 'Success Signin.!'
        }});
        console.log(result.user);
        this.getUserFromSession(result.user);
    }).catch((error) => {
        console.log(error);
        this.setState({response: {
          error: true,
          message: error.message
        }});
    });
  }
  logOut() {
    console.log('logOut');
    auth.signOut().then(() => {
        console.log(this.state.isAuthenticated);
        this.setState({ user: null });
        this.setState({ isAuthenticated: false });
        console.log(this.state.isAuthenticated);
    });
  }
  render() {
    return (
          <Container fluid >
            { !this.state.isLoading ?
            <Switch>
              <Route path="/login">
                {
                  !this.state.isAuthenticated ? (
                    <Signin login={this.login} loginGoogle={this.loginGoogle} response={this.state.response} />
                  ) : (
                    <Redirect
                      to={{
                        pathname: "/"
                      }}
                    />
                  )
                }
              </Route>
              <Route path="/register">
                {
                  !this.state.isAuthenticated ? (
                    <Signup />
                  ) : (
                    <Redirect
                      to={{
                        pathname: "/"
                      }}
                    />
                  )
                }
              </Route>
              <Route path="/">
                {
                  this.state.isAuthenticated ? (
                    <Main
                      user={this.state.user}
                      lasLogin={this.state.lasLogin}
                      logOut={this.logOut}
                      urlPicture={this.state.urlPicture}
                      urlWallpaper={this.state.urlWallpaper}
                      posts={this.state.posts}
                      addPost={this.addPost}
                      deletePost={this.deletePost}
                      friends={this.state.friends}
                    />
                  ) : (
                    <Redirect
                      to={{
                        pathname: "/login"
                      }}
                    />
                  )
                }
              </Route>
            </Switch> : <Loading/>
            }
            <Footer />
          </Container>
        )
  }
}

export default App;
